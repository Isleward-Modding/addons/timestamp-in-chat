// ==UserScript==
// @name         Isleward - Timestamp for messages in chat
// @namespace    Isleward.Addon
// @version      0.2
// @description  Shows timestamp
// @author       Qndel
// @match        play.isleward.com*
// @grant        none
// ==/UserScript==
function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                events.on('onGetMessages', this.onGetMessages.bind(this));
            },
            onGetMessages: function(obj) {
                if(obj.messages){
                    if(!obj.messages[0]){
                        obj.messages = [obj.messages];
                    }
                    if(obj.messages[0].message){
                        obj.messages[0].message = "["+(new Date().toLocaleTimeString()).split(":").join("&#58;")+"] "+obj.messages[0].message;
                    }
                }
            }
        });
    })
);